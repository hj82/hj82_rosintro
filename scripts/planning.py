#! /usr/bin/env python3

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
##

# Python 2/3 compatibility imports
from __future__ import print_function

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math
import time

# class that contains functions that enable path planning and execution.
class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        # group name is changed to manipulator
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    # Go to starting position by planning the path and executing it
    def go_to_start(self):
        # setting up the pose goal in cartesian coordinates for the staring position
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1.0
        pose_goal.position.x = 0.3
        pose_goal.position.y = -0.3
        pose_goal.position.z = 0.9

        self.move_group.set_pose_target(pose_goal)

        ## Now, we call the planner to compute the plan and execute it.
        plan = self.move_group.go(wait=True)
        # Calling `stop()` ensures that there is no residual movement
        self.move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets()
        self.move_group.clear_pose_targets()

    # this function is for drawing an initial, "H", starting from the initial pose.
    # given multiple waypoints, it plans and returns the cartesian path for the robot.
    def write_h(self, scale=1):
        # array to store each waypoints
        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        wpose.position.z -= scale * 0.26  # First down (z), starting from top left of the H, going to bottom left
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.13  # Second move up by half the distance (z), going to the middle point of the left bar of H
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * 0.18  # Third move sideways (y), drawing the middle bar of H
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.13  # Fourth move up by half distance (z), going to the top right of H
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.26  # Fifth move down (z), going to bottom right of H, finishing drawing the alphabet
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    # this function is for drawing an initial, "B", starting from the initial pose.
    # given multiple waypoints, it plans and returns the cartesian path for the robot.
    def write_b(self, scale=1):
        # array to store each waypoints
        waypoints = []
        # getting current pose
        wpose = self.move_group.get_current_pose().pose
        dist_y = 0.115 # length of the vertical bars of B
        wpose.position.y += scale * dist_y  # Move sideways (y), drawing the upper bar of B
        waypoints.append(copy.deepcopy(wpose))

        n = 10 # number of steps we will take to draw the round part of B
        r = 0.065 # radius of the round part of B
        dtheta = math.pi/n # step angle difference we will take when writing the round part of B
        ddisp = r*math.sin(dtheta/2)*2 # step distance we will take when writing the round part of B
        theta = 0 # initizing the angle, we define 0 to be pointing right
        for i in range(n): #  using a for loop to draw the round part of B
            theta += dtheta
            wpose.position.y += scale * ddisp * math.cos(theta)
            wpose.position.z -= scale * ddisp * math.sin(theta)
            waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.y -= scale * dist_y  # Move left (y), drawing the middle bar of B
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.y += scale * dist_y  # Move right (y), continue drawing the middle bar of B
        waypoints.append(copy.deepcopy(wpose))

        # repeatingsame step as previous to draw round part of B
        theta = 0 # initizing the angle, we define 0 to be pointing right
        for i in range(n): #  using a for loop to draw the round part of B
            theta += dtheta
            wpose.position.y += scale * ddisp * math.cos(theta)
            wpose.position.z -= scale * ddisp * math.sin(theta)
            waypoints.append(copy.deepcopy(wpose))

        wpose.position.y -= scale * 0.115  # Move left (y), drawing the middle bar of B
        waypoints.append(copy.deepcopy(wpose))
        # going back up is ommited, in this function, as using the function to go back to the start position will look like drawing the vertical bar

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    # this function is for drawing an initial, "J", starting from the initial pose.
    # given multiple waypoints, it plans and returns the cartesian path for the robot.
    def write_j(self, scale=1):
        # array to store each waypoints
        waypoints = []
        # getting current pose
        wpose = self.move_group.get_current_pose().pose
        wpose.position.y += scale * 0.18  # Move sideways (y), drawing the middle bar of J
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y -= scale * 0.09  # Move sideways (y) halfway back
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.16  # Go down (z) by certain distance for the middle of J
        waypoints.append(copy.deepcopy(wpose))

        n = 10 # number of steps we will take to draw the round part of J
        r = 0.1 # radius of the round part of J
        dtheta = math.pi/n # step angle difference we will take when writing the round part of J
        ddisp = r*math.sin(dtheta/2)*2 # step distance we will take when writing the round part of J
        theta = 0 # initizing the angle, we define 0 to be pointing downwards
        for i in range(n): #  using a for loop to draw the round part of J
            theta += dtheta
            wpose.position.y -= scale * ddisp * math.sin(theta)
            wpose.position.z -= scale * ddisp * math.cos(theta)
            waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    # executing the movement, given a plan compted by write functions
    def execute_plan(self, plan):
        # executing a plan
        self.move_group.execute(plan, wait=True)

    # printing current angles
    def print_angles(self):
        joint_angles = self.move_group.get_current_joint_values()
        for i in range(6):
            print(str(i+1)+': '+str(joint_angles[i]))

# main function
def main():
    try:
        # create an object to use functions to plan and execute paths
        tutorial = MoveGroupPythonInterfaceTutorial()
        # going to the start position
        tutorial.go_to_start()
        # printing current joint angles (key pose 1)
        print('joint angles for key pose 1')
        tutorial.print_angles()
        # planning and executing path to writing an H
        cartesian_plan, fraction = tutorial.write_h()
        tutorial.execute_plan(cartesian_plan)
        # printing current joint angles (key pose 2)
        print('joint angles for key pose 2')
        tutorial.print_angles()
        # going back to the start position
        tutorial.go_to_start()
        # planning and executing path to writing an B
        cartesian_plan, fraction = tutorial.write_b()
        tutorial.execute_plan(cartesian_plan)
        # going back to the start position
        tutorial.go_to_start()
        # planning and executing path to writing a J
        cartesian_plan, fraction = tutorial.write_j()
        tutorial.execute_plan(cartesian_plan)
        # printing current joint angles (key pose 3)
        print('joint angles for key pose 3')
        tutorial.print_angles()
        # sleeping for 1 sec before returning to initial position
        time.sleep(0.5)
        # going back to the start position
        tutorial.go_to_start()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()